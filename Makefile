# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

MK = $(BUILD).mk

install: makefile prefix
	@prefix=$$(cat .prefix) && \
	LD_LIBRARY_PATH="$${prefix}/lib:$${LD_LIBRARY_PATH}" \
	PATH="$${prefix}/bin:$${PATH}" \
	PKG_CONFIG_PATH="$${prefix}/lib/pkgconfig:$${PKG_CONFIG_PATH}" \
	TEXINPUTS="$${prefix}/share/tex:$${TEXINPUTS}" \
	$(MAKE) -f $(MK) -f Makefile install_all

clean: makefile
	@$(MAKE) -f $(MK) -f Makefile clean_all
	rm -rf .prefix $(MK)

distclean: makefile
	@$(MAKE) -f $(MK) -f Makefile distclean_all
	rm -rf .prefix src/*.sh.mk

uninstall: makefile
	@$(MAKE) -f $(MK) -f Makefile uninstall_all

# Generate the Makefile of the build
makefile:
	@echo "Setup $(MK)"
	@{ \
	  printf 'CACHE = %s\n' "$(CACHE)"; \
	  PATH="src:$${PATH}" USE_SIMD=$(USE_SIMD) \
	  $(SHELL) "$(BUILD)"; \
	} > "$(MK)"

prefix:
	@# store in .prefix the absolute prefix path
	@(p=$$(echo $(PREFIX)) && mkdir -p -- "$${p}" && cd -- "$${p}" && pwd) \
	> .prefix

include lint.mk

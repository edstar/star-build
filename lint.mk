# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

lint:
	shellcheck -o all -x -P src src/atrstm_0.1.1.sh
	shellcheck -o all -x -P src src/atrri_0.1.sh
	shellcheck -o all -x -P src src/atrtp_0.1.sh
	shellcheck -o all -x -P src src/aw_2.1.sh
	shellcheck -o all -x -P src src/build.sh
	shellcheck -o all -x -P src src/clipper2_1.1.1.sh
	shellcheck -o all -x -P src src/embree_4.0.1.sh
	shellcheck -o all -x -P src src/gmsh_4.12.2.sh
	shellcheck -o all -x -P src src/htcp_0.1.sh
	shellcheck -o all -x -P src src/htgop_0.2.sh
	shellcheck -o all -x -P src src/htmie_0.1.sh
	shellcheck -o all -x -P src src/htpp_0.5.sh
	shellcheck -o all -x -P src src/htrdr_0.10.sh
	shellcheck -o all -x -P src src/htsky_0.3.1.sh
	shellcheck -o all -x -P src src/libcyaml_1.3.1.sh
	shellcheck -o all -x -P src src/mrumtl_0.2.sh
	shellcheck -o all -x -P src src/noweb_2.13rc3.sh
	shellcheck -o all -x -P src src/profile.sh
	shellcheck -o all -x -P src src/polygon_0.2.sh
	shellcheck -o all -x -P src src/random123_1.14.0.sh
	shellcheck -o all -x -P src src/rnatm_0.1.sh
	shellcheck -o all -x -P src src/rngrd_0.1.sh
	shellcheck -o all -x -P src src/rnsf_0.1.sh
	shellcheck -o all -x -P src src/rnsl_0.1.sh
	shellcheck -o all -x -P src src/rsimd_0.5.sh
	shellcheck -o all -x -P src src/rsys_0.14.sh
	shellcheck -o all -x -P src src/scdoc_1.11.2-r1.sh
	shellcheck -o all -x -P src src/sgs.sh
	shellcheck -o all -x -P src src/sleef_3.5.2-r0.sh
	shellcheck -o all -x -P src src/star-2d_0.7.sh
	shellcheck -o all -x -P src src/star-3daw_0.5.sh
	shellcheck -o all -x -P src src/star-3d_0.10.sh
	shellcheck -o all -x -P src src/star-3dstl_0.4.sh
	shellcheck -o all -x -P src src/star-3dut_0.4.sh
	shellcheck -o all -x -P src src/star-aerosol_0.1.sh
	shellcheck -o all -x -P src src/star-buffer_0.1.sh
	shellcheck -o all -x -P src src/star-cad.sh
	shellcheck -o all -x -P src src/star-camera_0.2.sh
	shellcheck -o all -x -P src src/star-ck_0.1.sh
	shellcheck -o all -x -P src src/star-cmap_0.1.sh
	shellcheck -o all -x -P src src/star-enclosures-2d_0.6.sh
	shellcheck -o all -x -P src src/star-enclosures-3d_0.7.sh
	shellcheck -o all -x -P src src/star-engine_0.15.sh
	shellcheck -o all -x -P src src/star-geometry-3d_0.2.sh
	shellcheck -o all -x -P src src/star-mc_0.6.sh
	shellcheck -o all -x -P src src/star-mesh_0.1.sh
	shellcheck -o all -x -P src src/star-sf_0.9.sh
	shellcheck -o all -x -P src src/star-sp_0.14.sh
	shellcheck -o all -x -P src src/star-stl_0.5.sh
	shellcheck -o all -x -P src src/star-uvm_0.3.1.sh
	shellcheck -o all -x -P src src/star-vx_0.3.sh
	shellcheck -o all -x -P src src/star-wf_0.0.sh
	shellcheck -o all -x -P src src/stardis_0.10.1.sh
	shellcheck -o all -x -P src src/stardis-green_0.5.1.sh
	shellcheck -o all -x -P src src/stardis-solver_0.15.2.sh

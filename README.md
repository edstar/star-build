# Star-Build

Automation tool that builds and installs programs and libraries and
their dependencies from source code. Pre-compiled binaries can also be
deployed.

## Requirements

To use Star-Build you need a POSIX implementation of make and a POSIX
compliant shell. The remaining dependencies depend on the programs and
libraries to be build and installed. However, you can assume that the
`curl`, `git`, `sha512sum` and `tar` utilities must be installed, as
well as a C99 compiler and C++11 compiler.

## Running star-build

Edit `config.mk` as needed, then run:

    make

Delete installed content:

    make uninstall

Clean up intermediary files:

    make clean

Deep cleaning, i.e. removal of intermediate files and cached data:

    make distclean

## Copyright

Copyright © 2023, 2024 |Méso|Star> (contact@meso-star.com)

## License

It is free software released under the GPL v3+ license: GNU GPL version
3 or later. You are welcome to redistribute them under certain
conditions; refer to the COPYING file for details.

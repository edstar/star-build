#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${STAR_SP_SH}" ] && return
export STAR_SP_SH=1

. "build.sh"
. "random123_1.14.0.sh"
. "rsys_0.14.sh"

name="star-sp"
url="$\(REPO\)/star-sp.git"
tag="0.14"
dep="random123 rsys"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
git_repo

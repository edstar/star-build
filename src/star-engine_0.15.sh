#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${STAR_ENGINE_SH}" ] && return
export STAR_ENGINE_SH=1

[ -z "${USE_SIMD}" ] && USE_SIMD="0"

. "aw_2.1.sh"
. "polygon_0.2.sh"
. "rsys_0.14.sh"
. "star-2d_0.7.sh"
. "star-3d_0.10.sh"
. "star-3daw_0.5.sh"
. "star-3dstl_0.4.sh"
. "star-3dut_0.4.sh"
. "star-sf_0.9.sh"
. "star-sp_0.14.sh"
. "star-stl_0.5.sh"
. "star-mc_0.6.sh"
. "star-uvm_0.3.1.sh"
. "star-vx_0.3.sh"

[ "${USE_SIMD}" != "0" ] && . "rsimd_0.5.sh"

profile star-engine.profile

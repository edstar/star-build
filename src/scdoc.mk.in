# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Configuration macros
SCDOC_TAG = @TAG@
SCDOC_URL = https://git.sr.ht/~sircmpwn/scdoc

# Helper macros
SCDOC_DIR = $(CACHE)/scdoc/$(SCDOC_TAG)

scdoc: build_scdoc
	@prefix=$$(cat .prefix) && \
	cd -- "$(SCDOC_DIR)" && $(MAKE) PREFIX="$${prefix}" install

build_scdoc: fetch_scdoc prefix
	@cd -- "$(SCDOC_DIR)" && $(MAKE)

$(SCDOC_DIR):
	@git clone $(SCDOC_URL) "$@"

fetch_scdoc: $(SCDOC_DIR)
	@cd -- "$(SCDOC_DIR)" && \
	git fetch origin && \
	git checkout -B star-build && \
	git reset --hard $(SCDOC_TAG)

clean_scdoc:
	if [ -d "$(SCDOC_DIR)" ]; then \
		cd -- "$(SCDOC_DIR)" && $(MAKE) clean; \
	fi

uninstall_scdoc: fetch_scdoc prefix
	@prefix=$$(cat .prefix) && \
	cd -- "$(SCDOC_DIR)" && $(MAKE) PREFIX="$${prefix}" uninstall

distclean_scdoc:
	rm -rf "$(SCDOC_DIR)"

clean_all: clean_scdoc
distclean_all: distclean_scdoc
install_all: scdoc
uninstall_all: uninstall_scdoc

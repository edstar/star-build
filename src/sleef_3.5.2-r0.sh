#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${SLEEF_SH}" ] && return
export SLEEF_SH=1

tag=85440a5e87dae36ca1b891de14bc83b441ae7c43

sed "s/@TAG@/${tag}/" "src/sleef.mk.in"

# Add the template file as a prerequisite for the script invoked
# i.e. the build file
printf "%s: src/sleef.mk.in\n" "$0"

#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${STARDIS_SOLVER}" ] && return
export STARDIS_SOLVER=1

[ -z "${DISTRIB_PARALLELISM}" ] && DISTRIB_PARALLELISM=MPI

. "build.sh"
. "rsys_0.14.sh"
. "star-2d_0.7.sh"
. "star-3d_0.10.sh"
. "star-enclosures-2d_0.6.sh"
. "star-enclosures-3d_0.7.sh"
. "star-sp_0.14.sh"
. "star-wf_0.0.sh"

name="stardis-solver"
url="$\(REPO\)/stardis-solver.git"
tag="0.15.2"
dep="rsys star-2d star-3d star-enclosures-2d star-enclosures-3d star-sp star-wf"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
opt="${opt} DISTRIB_PARALLELISM=${DISTRIB_PARALLELISM}"
git_repo

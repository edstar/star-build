#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

# Build for the dependencies of the Star Geometric Sensitivity project

[ -n "${SGS_SH}" ] && return
export SGS_SH=1

. "build.sh"

. "noweb_2.13rc3.sh"
. "rsys_0.14.sh"
. "star-3d_0.10.sh"
. "star-sp_0.14.sh"
. "star-mc_0.6.sh"

# Setup make directives for the profile file
profile sgs.profile

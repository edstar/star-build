#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${RNATM_SH}" ] && return
export RNATM_SH=1

. "build.sh"
. "rnsf_0.1.sh"
. "rnsl_0.1.sh"
. "rsys_0.14.sh"
. "star-aerosol_0.1.sh"
. "star-buffer_0.1.sh"
. "star-ck_0.1.sh"
. "star-mesh_0.1.sh"
. "star-sf_0.9.sh"
. "star-uvm_0.3.1.sh"
. "star-vx_0.3.sh"

name="rnatm"
url="$\(REPO\)/rnatm.git"
tag="0.1"
dep="rnsf rnsl rsys star-aerosol star-buffer star-ck star-mesh star-sf \
 star-uvm star-vx"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
git_repo

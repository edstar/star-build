#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${ACONT_SH}" ] && return
export ACONT_SH=1

name="4acont"
url="https://gitlab.com/yanissnp/continuum_4A.git"
tag="0.0.1"
dep=""
opt="-DCMAKE_BUILD_TYPE=$\(BUILD_TYPE\)"
opt="${opt} -DCMAKE_INSTALL_LIBDIR=lib"
cmake_dir="cmake"

sed \
  -e "s#@NAME@#${name}#g" \
  -e "s#@URL@#${url}#g" \
  -e "s#@TAG@#${tag}#g" \
  -e "s#@DEP@#${dep}#g" \
  -e "s#@OPT@#${opt}#g" \
  -e "s#@CMAKEDIR@#${cmake_dir}#g" \
  "src/cmake.mk.in"

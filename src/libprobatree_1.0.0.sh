#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${LIBPROBATREE_SH}" ] && return
export LIBPROBATREE_SH=1

name="libprobatree"
url="https://gitlab.com/mbati/libprobatree.git"
tag="1.0.1"
dep="lblu"
opt="-DCMAKE_BUILD_TYPE=$\(BUILD_TYPE\)"
opt="${opt} -DBUILD_TESTS=OFF"
cmake_dir="."

sed \
  -e "s#@NAME@#${name}#g" \
  -e "s#@URL@#${url}#g" \
  -e "s#@TAG@#${tag}#g" \
  -e "s#@DEP@#${dep}#g" \
  -e "s#@OPT@#${opt}#g" \
  -e "s#@CMAKEDIR@#${cmake_dir}#g" \
  "src/cmake.mk.in"

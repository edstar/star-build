#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${RADFORCE_SH}" ] && return
export RADFORCE_SH=1

. "build.sh"
. "lblu_0.0.1.sh"
. "libprobatree_1.0.0.sh"
. "4acont_0.0.1.sh"
. "rsys_0.14.sh"
. "star-mc_0.6.sh"
. "star-sf_0.9.sh"
. "star-sp_0.14.sh"

name="radforce"
url="https://gitlab.com/yanissnp/RadForcE.git"
tag="0.0.1"
dep="lblu libprobatree 4acont rsys star-mc star-sf star-sp"
opt="-DCMAKE_BUILD_TYPE=$\(BUILD_TYPE\)"
cmake_dir="cmake"

sed \
  -e "s#@NAME@#${name}#g" \
  -e "s#@URL@#${url}#g" \
  -e "s#@TAG@#${tag}#g" \
  -e "s#@DEP@#${dep}#g" \
  -e "s#@OPT@#${opt}#g" \
  -e "s#@CMAKEDIR@#${cmake_dir}#g" \
  "src/cmake.mk.in"

profile radforce.profile

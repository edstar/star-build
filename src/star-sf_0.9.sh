#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${STAR_SF_SH}" ] && return
export STAR_SF_SH=1

. "build.sh"
. "rsys_0.14.sh"
. "star-sp_0.14.sh"

[ -z "${USE_SIMD}" ] && USE_SIMD="0"
[ "${USE_SIMD}" != 0 ] && . "rsimd_0.5.sh"

name="star-sf"
url="$\(REPO\)/star-sf.git"
tag="0.9"
dep="rsys star-sp"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"

if [ "${USE_SIMD}" = "0" ]; then
  opt="${opt} SIMD_WIDTH=NONE"
else
  dep="${dep} rsimd"
fi

git_repo

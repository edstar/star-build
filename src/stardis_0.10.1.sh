#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${STARDIS}" ] && return
export STARDIS=1

[ -z "${DISTRIB_PARALLELISM}" ] && DISTRIB_PARALLELISM=MPI

. "build.sh"
. "rsys_0.14.sh"
. "star-3d_0.10.sh"
. "star-enclosures-3d_0.7.sh"
. "star-geometry-3d_0.2.sh"
. "star-sp_0.14.sh"
. "star-stl_0.5.sh"
. "stardis-solver_0.15.2.sh"

name="stardis"
url="$\(REPO\)/stardis.git"
tag="0.10.1"
dep="rsys star-3d star-enclosures-3d star-geometry-3d star-sp star-stl stardis-solver"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
opt="${opt} DISTRIB_PARALLELISM=${DISTRIB_PARALLELISM}"
git_repo

profile stardis.profile

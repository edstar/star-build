#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${HTRDR_SH}" ] && return
export HTRDR_SH=1

[ -z "${ATMOSPHERE}" ] && ATMOSPHERE=ENABLE
[ -z "${COMBUSTION}" ] && COMBUSTION=ENABLE
[ -z "${PLANETO}" ] && PLANETO=ENABLE

. "build.sh"
. "aw_2.1.sh"
. "htpp_0.5.sh"
. "mrumtl_0.2.sh"
. "rsys_0.14.sh"
. "star-3d_0.10.sh"
. "star-camera_0.2.sh"
. "star-sf_0.9.sh"
. "star-sp_0.14.sh"
. "star-vx_0.3.sh"

if [ "${ATMOSPHERE}" = "ENABLE" ]; then
  . "htsky_0.3.1.sh"
  dep="${dep} htsky"
fi
if [ "${COMBUSTION}" = "ENABLE" ]; then
  . "atrstm_0.1.1.sh"
  dep="${dep} atrstm"
fi
if [ "${PLANETO}" = "ENABLE" ]; then
  . "rnatm_0.1.sh"
  . "rngrd_0.1.sh"
  . "star-buffer_0.1.sh"
  dep="${dep} rnatm rngrd star-buffer"
fi

opt="ATMOSPHERE=${ATMOSPHERE} COMBUSTION=${COMBUSTION} PLANETO=${PLANETO}"

name="htrdr"
url="$\(REPO\)/htrdr.git"
tag="0.10"
dep="${dep} aw mrumtl rsys star-3d star-camera star-sf star-sp star-vx"
opt="${opt} BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
git_repo

profile htrdr.profile

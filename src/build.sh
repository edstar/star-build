#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${BUILD_SH}" ] && return
export BUILD_SH=1

# Generate the targets for a git repository. The input variables are:
#   - name : the name of the target (required)
#   - url : the URL of the git repository (required)
#   - tag : the tag/branch/commit to use (required)
#   - dep : name of the targets on which this build depends (optional)
#   - opt : the options passed when invoking make (optional)
git_repo()
{
  sed \
    -e "s#@NAME@#${name}#g" \
    -e "s#@URL@#${url}#g" \
    -e "s#@TAG@#${tag}#g" \
    -e "s#@DEP@#${dep}#g" \
    -e "s#@OPT@#${opt}#g" \
    "src/git.mk.in"

  # Reset variables
  name=""
  url=""
  tag=""
  dep=""
  opt=""
}

# Generate the targets for a Star-PKG binary. The input variables are:
#   - name: the name of the target (required)
#   - url: the URL of the package (required)
bin_spkg()
{
  arch="${url##*/}"
  arch="${arch%.*}"
  path="${url%/*}"

  sed \
    -e "s#@NAME@#${name}#g" \
    -e "s#@ARCH@#${arch}#g" \
    -e "s#@PATH@#${path}#g" \
    "src/spkg.mk.in"

  # Reset variables
  name=""
  url=""
}

# Generate the targets of a profile
profile()
{
  sed -e "s#@NAME@#$1#g" "src/profile.mk.in"
}

#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${RNGRD_SH}" ] && return
export RNGRD_SH=1

. "build.sh"
. "mrumtl_0.2.sh"
. "rnsl_0.1.sh"
. "rsys_0.14.sh"
. "star-3d_0.10.sh"
. "star-buffer_0.1.sh"
. "star-mesh_0.1.sh"
. "star-sf_0.9.sh"

name="rngrd"
url="$\(REPO\)/rngrd.git"
tag="0.1"
dep="mrumtl rnsl rsys star-3d star-buffer star-mesh star-sf"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
git_repo

#!/bin/sh

# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

[ -n "${STAR_CAD_SH}" ] && return
export STAR_CAD_SH=1

. "build.sh"
. "gmsh_4.12.2.sh"
. "rsys_0.14.sh"
. "star-enclosures-3d_0.7.sh"
. "star-geometry-3d_0.2.sh"

name="star-cad"
url="$\(REPO\)/star-cad.git"
tag="origin/develop"
dep="gmsh rsys star-enclosures-3d star-geometry-3d"
opt="BUILD_TYPE=$\(BUILD_TYPE\) LIB_TYPE=$\(LIB_TYPE\)"
git_repo

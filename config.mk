# Copyright (C) 2023, 2024 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Project to build. To see the available projects, list the shell
# scripts in
# the src directory:
# 	 find src/ -name "*.sh" ! -name "build.sh" | sort
BUILD = src/star-engine_0.15.sh

# Installation path
PREFIX = ./local

# Are SIMD instruction sets enabled?
USE_SIMD = 0

# How to build programs and libraries, typically in RELEASE or in DEBUG
BUILD_TYPE = RELEASE

# Type of library to build, either SHARED or STATIC
LIB_TYPE = SHARED

# Cache directory, i.e. temporary directory into which temporary
# resources are downloaded and sources compiled before installation.
CACHE = ./cache

# List of repositories
REPO = https://gitlab.com/meso-star
REPO_BIN = https://www.meso-star.com/packages/v0.4/
REPO_VAPLV = https://gitlab.com/vaplv
